package presentation;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import businessLogic.ProdusBLL;
import model.Produs;


public class StergereProdus extends JFrame{

	private JLabel idProdusLabel;
	private JLabel numeLabel;
	private JLabel observatiiLabel;
	
	private JTextField idProdusTxt;
	private JTextField numeTxt;
	private JTextField observatiiTxt;
	
	private JButton deleteProdusBtn;
	
	private JPanel pane = new JPanel(new GridBagLayout());
	GridBagConstraints c = new GridBagConstraints();

	public StergereProdus(){
		super("Sterge produs");
		c.fill=GridBagConstraints.HORIZONTAL;
		pane.setBackground (new Color(255,51,51));
		
		idProdusLabel = new JLabel("Introduceti id-ul :");
		idProdusLabel.setForeground(Color.white);
		
		idProdusTxt = new JTextField(40);
		
		numeLabel = new JLabel("Introduceti numele :");
		numeLabel.setForeground(Color.white);
		
		numeTxt = new JTextField(40);
		
		deleteProdusBtn = new JButton("Sterge");
		deleteProdusBtn.setBackground(new Color(255,204,0));
		deleteProdusBtn.setForeground(Color.black);
		
		c.gridx = 0;c.gridy = 0;
		pane.add(idProdusLabel,c);
		c.gridx = 0;c.gridy = 1;
		pane.add(idProdusTxt,c);
		c.gridx = 0;c.gridy = 2;
		pane.add(numeLabel,c);
		c.gridx = 0;c.gridy = 3;
		pane.add(numeTxt,c);
		c.gridx = 0;c.gridy = 4;
		pane.add(deleteProdusBtn,c);
		
		c.gridx = 0;c.gridy = 5;
		observatiiLabel=new JLabel("Observatii: ");
		observatiiLabel.setForeground(Color.cyan);
		pane.add(observatiiLabel,c);
		
		c.gridx = 0;c.gridy = 6;
		observatiiTxt=new JTextField(40);
		pane.add(observatiiTxt,c);
		observatiiTxt.setEditable(false);
		
		this.add(pane);
		init();
	}
	
	private void init(){
		
		this.setVisible(true);
		this.setSize(220, 200);
		this.setLocation(470, 20);
		
		deleteProdusBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				int validare=1;
				String informatie=null;	

				ProdusBLL produsBll = new ProdusBLL();		
				Produs produsCurent = new Produs();
				
				if(idProdusTxt.getText().isEmpty() || numeTxt.getText().isEmpty()) {
					informatie="campuri goale";
					validare=0;
				}else {
				if(Integer.parseInt(idProdusTxt.getText())<=0 ){
				    informatie="id produs < 0";
					validare=0;
				}		
				}
				
				observatiiTxt.setText("");
				 if(validare==0)  {
				        observatiiTxt.setText(informatie);
				        observatiiTxt.setForeground(Color.red);
				        }else {
				try {
					produsCurent = produsBll.delete(Integer.parseInt(idProdusTxt.getText()),numeTxt.getText());

				} catch (Exception ex) {
					System.out.println(ex.getMessage());
				}
				observatiiTxt.setText("Date valide");
				observatiiTxt.setForeground(Color.GREEN);
			}
			}
			});
	}
}