package presentation;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import businessLogic.ComandaBLL;
import businessLogic.ProdusBLL;
import model.Produs;

public class AdaugareComanda extends JFrame{

	private JLabel idClientLabel;
	private JLabel idProdusLabel;
	private JLabel cantitateLabel;
	private JLabel idComandaLabel;
	private JLabel observatiiLabel;
	
	private JTextField idClientTxt;	
	private JTextField idProdusTxt;
	private JTextField cantitateTxt;
	private JTextField idComandaTxt;
	private JTextField observatiiTxt;
	
	private JButton adaugaComandaBtn;
    private JPanel pane = new JPanel(new GridBagLayout());
	
	GridBagConstraints c = new GridBagConstraints();
	public AdaugareComanda(){
		super("Adauga comanda");
		c.fill=GridBagConstraints.HORIZONTAL;
		pane.setBackground (new Color(0,0,204));
		
		idClientLabel = new JLabel("Dati id-ul clientului:");
		idClientLabel.setForeground(Color.white);
		idClientTxt = new JTextField(40);
		
		idProdusLabel = new JLabel("Dati id-ul produsului:");
		idProdusLabel.setForeground(Color.white);
		idProdusTxt = new JTextField(40);
		
		cantitateLabel = new JLabel("Dati cantitatea produsului:");
		cantitateLabel.setForeground(Color.white);
		cantitateTxt = new JTextField(40);
		
		idComandaLabel = new JLabel("Dati id-ul comenzii:");
		idComandaLabel.setForeground(Color.white);
		idComandaTxt= new JTextField(40);
		
		adaugaComandaBtn = new JButton("Comanda");
		
		c.gridx = 0;c.gridy = 0;
		pane.add(idClientLabel,c);
		c.gridx = 0;c.gridy = 1;
		pane.add(idClientTxt,c);
		c.gridx = 0;c.gridy = 2;
		pane.add(idProdusLabel,c);
		c.gridx = 0;c.gridy = 3;
		pane.add(idProdusTxt,c);
		c.gridx = 0;c.gridy = 4;
		pane.add(cantitateLabel,c);
		c.gridx = 0;c.gridy = 5;
		pane.add(cantitateTxt,c);
		c.gridx = 0;c.gridy = 6;
		pane.add(idComandaLabel,c);
		c.gridx = 0;c.gridy = 7;
		pane.add(idComandaTxt,c);
		c.gridx = 0;c.gridy = 8;
		pane.add(adaugaComandaBtn,c);
		
		c.gridx = 0;c.gridy = 9;
		observatiiLabel=new JLabel("Observatii: ");
		observatiiLabel.setForeground(Color.cyan);
		pane.add(observatiiLabel,c);
		
		c.gridx = 0;c.gridy = 10;
		observatiiTxt=new JTextField(40);
		pane.add(observatiiTxt,c);
		observatiiTxt.setEditable(false);
		
		this.add(pane);
		init();
	}
	
	private void init(){
		
		this.setVisible(true);
		this.setSize(300, 250);
		this.setLocation(470, 200);		

		adaugaComandaBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				int validare=1;
				String informatie=null;	
				ComandaBLL comandaBll = new ComandaBLL();
				ProdusBLL produsBll = new ProdusBLL();
				Produs produsCurent = new Produs();
				
				if(idClientTxt.getText().isEmpty()||idProdusTxt.getText().isEmpty()||cantitateTxt.getText().isEmpty() 
				||idComandaTxt.getText().isEmpty()){
					informatie="campuri goale";
					validare=0;
				}else {
				if(Integer.parseInt(idProdusTxt.getText())<=0 ){
				    informatie="id produs < 0";
					validare=0;
				}
				if(Integer.parseInt(idClientTxt.getText())<=0 ) {
					informatie="id client < 0";
					validare=0;		   	
				}
				if(Integer.parseInt(cantitateTxt.getText())<=0) {
					informatie="cantitate < 0";
					validare=0;	
				}
				if(Integer.parseInt(idComandaTxt.getText())<=0) {
					informatie="id comanda < 0";
					validare=0;
				}
				}
				
				observatiiTxt.setText("");
				 if(validare==0)  {
				        observatiiTxt.setText(informatie);
				        observatiiTxt.setForeground(Color.red);
				        }else {
				try {
					
					produsCurent = produsBll.findById(Integer.parseInt(idProdusTxt.getText().trim()));
					
					if( produsCurent.getCantitate() - Integer.parseInt(cantitateTxt.getText()) >=0 ){	
						
						try
						{   
						    produsBll.update(produsCurent.getIdProdus(),produsCurent.getDenumire(),produsCurent.getMarca(),produsCurent.getCantitate()-Integer.parseInt(cantitateTxt.getText()),produsCurent.getPret(),produsCurent.getIdProdus());
						   			
						}
						catch (Exception e1)
						{
							e1.getMessage();
						}
						finally
						{
							comandaBll.comandaNoua(Integer.parseInt(idClientTxt.getText()), Integer.parseInt(idProdusTxt.getText()),Integer.parseInt(cantitateTxt.getText()),Integer.parseInt(idComandaTxt.getText()));
						       
						}
						
					}
					else
						JOptionPane.showMessageDialog(null, "Nu sunt suficiente produse pe stoc!");
				} catch (Exception ex) {
					System.out.println(ex.getMessage());
				}
				observatiiTxt.setText("Date valide");
				observatiiTxt.setForeground(Color.GREEN);
			}
			}
		});
		
	}
	
}
