package presentation;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import businessLogic.ClientBLL;
import model.Client;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class AdaugareClient extends JFrame{

	private static final long serialVersionUID = 1L;
	private JPanel pane = new JPanel(new GridBagLayout());
	GridBagConstraints c = new GridBagConstraints();
	
	private JLabel idClientLabel;
	private JLabel numeClientLabel;
	private JLabel varstaClientLabel;
	private JLabel observatiiLabel;
	
	private JTextField idClientTxt;
	private JTextField numeClientTxt;
	private JTextField varstaClientTxt;
	private JTextField observatiiTxt;
	
	private JButton adaugaClientBtn;
	
	public AdaugareClient(){
		super("Adauga client");
		c.fill=GridBagConstraints.HORIZONTAL;
		pane.setBackground (new Color(0,102,0));
		
		c.gridx = 0;c.gridy = 0;
		idClientLabel = new JLabel("Introduceti id-ul clientului: ");
		idClientLabel.setForeground(Color.white);
		pane.add(idClientLabel, c);
		
		c.gridx = 0;c.gridy = 1;
		idClientTxt = new JTextField(40);
		pane.add(idClientTxt, c);
		
		c.gridx = 0;c.gridy = 2;
		numeClientLabel = new JLabel("Introduceti numele clientului: ");
		numeClientLabel.setForeground(Color.white);
		pane.add(numeClientLabel, c);
		
		c.gridx = 0;c.gridy = 3;
		numeClientTxt = new JTextField(40);	
		pane.add(numeClientTxt, c);
		
		c.gridx = 0;c.gridy = 4;
		varstaClientLabel = new JLabel("Introduceti varsta clientului: ");
		varstaClientLabel.setForeground(Color.white);
		pane.add(varstaClientLabel, c);
		
		c.gridx = 0;c.gridy = 5;
		varstaClientTxt = new JTextField(40);
		pane.add(varstaClientTxt, c);
		
		c.gridx = 0;c.gridy = 6;
		adaugaClientBtn = new JButton("Adauga");
		adaugaClientBtn.setBackground(new Color(51,51,51));
		adaugaClientBtn.setForeground(Color.white);
		pane.add(adaugaClientBtn,c);
		
		c.gridx = 0;c.gridy = 7;
		observatiiLabel=new JLabel("Observatii: ");
		observatiiLabel.setForeground(Color.cyan);
		pane.add(observatiiLabel,c);
		
		c.gridx = 0;c.gridy = 8;
		observatiiTxt=new JTextField(40);
		pane.add(observatiiTxt,c);
		observatiiTxt.setEditable(false);
		
		this.add(pane);
		
		Toolkit tk = Toolkit.getDefaultToolkit();
	    Dimension screenSize = tk.getScreenSize();
		 int screenHeight = screenSize.height;
		 int screenWidth = screenSize.width;	   
		 this.setLocation(screenWidth/4 , screenHeight / 2);
		init();
		
	}
	
	private void init(){
		
		this.setVisible(true);
		this.setSize(300, 220);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		adaugaClientBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				int validare=1;
				String informatie=null;			
				ClientBLL clientBll = new ClientBLL();
				Client clientCurent = new Client();
				
				if(numeClientTxt.getText().isEmpty() || varstaClientTxt.getText().isEmpty() ||
						idClientTxt.getText().isEmpty()) {
					informatie="campuri goale";
					validare=0;
				}else {
				if(Integer.parseInt(varstaClientTxt.getText())<=0 ||Integer.parseInt(varstaClientTxt.getText())>=120){
				    informatie="varsta invalida";
					validare=0;
				}
				if(Integer.parseInt(idClientTxt.getText())<=0) {
					informatie="id Client < 0";
					validare=0;
				}
				}
						
			    observatiiTxt.setText("");
			   
		        if(validare==0)  {
			        observatiiTxt.setText(informatie);
			        observatiiTxt.setForeground(Color.red);
			        }else {
					
				try {
					clientCurent = clientBll.insert(Integer.parseInt(idClientTxt.getText()),
							numeClientTxt.getText(),Integer.parseInt(varstaClientTxt.getText()));
					
				} catch (Exception ex) {
					System.out.println(ex.getMessage());
				}
				observatiiTxt.setText("Date valide");
				observatiiTxt.setForeground(Color.GREEN);
			        }
		        
			}
			});
	}
	
}
