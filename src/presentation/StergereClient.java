package presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import businessLogic.ClientBLL;
import model.Client;


public class StergereClient extends JFrame {

	private JLabel idClientLabel;
	private JLabel numeLabel;
	private JLabel observatiiLabel;
	
	private JTextField idClientTxt;
	private JTextField numeTxt;
	private JTextField observatiiTxt;
	
	private JButton deleteClientBtn;
	private JPanel pane = new JPanel(new GridBagLayout());
	GridBagConstraints c = new GridBagConstraints();
	
	public StergereClient(){
		super("Sterge client");
		pane.setBackground(new Color(153,0,0));
		
		numeLabel = new JLabel("Dati numele clientului: ");
		numeTxt = new JTextField(40);
		idClientLabel = new JLabel("Dati id-ul clientului: ");
		idClientTxt = new JTextField(40);
		deleteClientBtn = new JButton("Sterge");
		
		c.fill=GridBagConstraints.HORIZONTAL;
		
		c.gridx = 0;c.gridy = 0;
		idClientLabel.setForeground(Color.white);
		pane.add(idClientLabel, c);
		
		c.gridx = 0;c.gridy = 1;
		pane.add(idClientTxt,c);
		
		c.gridx = 0;c.gridy = 2;
		pane.add(numeLabel, c);
		numeLabel.setForeground(Color.white);
		
		c.gridx = 0;c.gridy = 3;
		pane.add(numeTxt,c);
		
		c.gridx = 0;c.gridy = 4;
		deleteClientBtn.setBackground(new Color(153,102,0));
		deleteClientBtn.setForeground(Color.white);
		pane.add(deleteClientBtn,c);
		
		c.gridx = 0;c.gridy = 5;
		observatiiLabel=new JLabel("Observatii: ");
		observatiiLabel.setForeground(Color.cyan);
		pane.add(observatiiLabel,c);
		
		c.gridx = 0;c.gridy = 6;
		observatiiTxt=new JTextField(40);
		pane.add(observatiiTxt,c);
		observatiiTxt.setEditable(false);
		
		this.add(pane);
		Toolkit tk = Toolkit.getDefaultToolkit();
	    Dimension screenSize = tk.getScreenSize();
		 int screenHeight = screenSize.height;
		 int screenWidth = screenSize.width;	   
		 this.setLocation(screenWidth/2 , screenHeight / 2);
		init();
	}
	
	private void init(){
			
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(240, 180);
		this.setVisible(true);
		
		deleteClientBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				int validare=1;
				String informatie=null;		
				ClientBLL clientBll = new ClientBLL();		
				Client clientCurent = new Client();
				
				if(idClientTxt.getText().isEmpty() || numeTxt.getText().isEmpty()) {
					informatie="campuri goale";
					validare=0;
				}else {
				if(Integer.parseInt(idClientTxt.getText())<=0 ){
				    informatie="id produs < 0";
					validare=0;
				}		
				}
				
				observatiiTxt.setText("");
				   
			    if(validare==0)  {
			        observatiiTxt.setText(informatie);
			        observatiiTxt.setForeground(Color.red);
			        }else {
				try {
					clientCurent = clientBll.delete(Integer.parseInt(idClientTxt.getText()),
							numeTxt.getText());

				} catch (Exception ex) {
					System.out.println(ex.getMessage());
				}
				observatiiTxt.setText("Date valide");
				observatiiTxt.setForeground(Color.GREEN);
			}
			}
			});
	}
	
}
