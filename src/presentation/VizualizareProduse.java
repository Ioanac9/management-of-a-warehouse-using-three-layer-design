package presentation;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import businessLogic.ProdusBLL;
import model.Produs;

public class VizualizareProduse extends JFrame {

	private JScrollPane s ;
	private JPanel p;
	private JPanel pane = new JPanel(new GridBagLayout());
	GridBagConstraints c = new GridBagConstraints();
	
	public VizualizareProduse() throws SQLException{
		super("Baza de date Produse");
		Object[] names = { "idProdus","Denumire","Marca","Cantitate","Pret"};
		List<Produs> produs1 = new ArrayList<Produs>();
		List<Object> cnt = new ArrayList<Object>();
		ProdusBLL produsBll = new ProdusBLL();
		
		pane.setBackground (new Color(102,0,153));
		c.fill=GridBagConstraints.HORIZONTAL;	
		
		try {
				produs1 = produsBll.selectAll();//se selecteaza toate produsele
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(null,ex.getMessage());
		}
		
		for(Produs it : produs1)
			cnt.add(it); //pentru fiecare obiect din lista de produse se adauga intr-o lista 
			
		s = new JScrollPane(Reflection.retrieveProperties(cnt,names)); //se creeaza tabelul cu datele din tabela de produse
		c.gridx = 0;	c.gridy = 0;
		pane.add(s,c);
		this.add(pane);
		this.setVisible(true);
		this.setSize(500, 500);
		this.setLocation(500,100);
	}
	
}