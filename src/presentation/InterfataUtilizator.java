package presentation;


import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class InterfataUtilizator extends JFrame{

	private JButton adaugaClient;
	private JButton editeazaClient;
	private JButton stergeClient;
	private JButton afiseazaClienti;
	private JLabel produs;
	private JButton adaugaProdus;
	private JButton editeazaProdus;
	private JButton stergeProdus;
	private JButton afiseazaProdus;
	private JButton afiseazaComenzi;
	private JButton comanda;
	private JPanel pane = new JPanel(new GridBagLayout());
	GridBagConstraints c = new GridBagConstraints();
	
	public InterfataUtilizator(){
		super("Operatii clienti");  
		pane.setBackground (new Color(102,0,153));
		adaugaClient = new JButton("Adauga Client");
		editeazaClient = new JButton("Editeaza Client");
		stergeClient = new JButton("Sterge Client");
		afiseazaClienti = new JButton("Afiseaza Clienti");
		adaugaProdus = new JButton("Adauga Produs");
		editeazaProdus = new JButton("Editeaza Produs");
		stergeProdus = new JButton("Sterge Produs");
		afiseazaProdus = new JButton("Afiseaza Produse");
		afiseazaComenzi = new JButton("Afiseaza Comenzi");
		comanda = new JButton("Adauga Comanda");
		
		c.fill=GridBagConstraints.HORIZONTAL;
		
		c.gridx = 0;c.gridy = 0;
		pane.add(adaugaClient, c);
		c.gridx = 1;	c.gridy = 0;
		pane.add(editeazaClient,c);
		c.gridx = 2;c.gridy = 0;
		pane.add(stergeClient, c);
		c.gridx = 3;	c.gridy = 0;
		pane.add(afiseazaClienti,c);
		
		c.gridx = 0;	c.gridy = 1;
		pane.add(adaugaProdus,c);
		c.gridx = 1;	c.gridy = 1;
		pane.add(editeazaProdus,c);
		c.gridx = 2;	c.gridy = 1;
		pane.add(stergeProdus,c);
		c.gridx = 3;	c.gridy = 1;
		pane.add(afiseazaProdus,c);
		
		c.insets = new Insets(10,10,10,110);
		c.gridx = 2;	c.gridy = 5;
		
		pane.add(comanda,c);
		 c.gridx=2; c.gridy=6;
		 pane.add(afiseazaComenzi,c);
		
		
		this.add(pane);
		this.setVisible(true);
		this.setSize(700, 200);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);   
		this.setLocation(333 ,230);
		initiate();
	
	}
	
	private void initiate(){
	adaugaClient.addActionListener(new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			
			AdaugareClient addC= new AdaugareClient();
			addC.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
			
		}
		});
	
	editeazaClient.addActionListener(new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			
			EditareClient editC = new EditareClient();
			editC.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			
		}
		});
	
	
	stergeClient.addActionListener(new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			
			StergereClient delC = new StergereClient();
			delC.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			
		}
		});
	
	afiseazaClienti.addActionListener(new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			try {		
				VizualizareClienti viewC = new VizualizareClienti();
				viewC.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			
		}
		});
afiseazaComenzi.addActionListener(new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			try {
				VizualizareComenzi viewCm = new VizualizareComenzi();
				viewCm.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			
		}
		});
	
	
	adaugaProdus.addActionListener(new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			
			AdaugareProdus addP = new AdaugareProdus();
			addP.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			
		}
		});
	
	editeazaProdus.addActionListener(new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			
			EditareProdus editP = new EditareProdus();
			editP.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			
		}
		});
	
	stergeProdus.addActionListener(new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			
			StergereProdus delP = new StergereProdus();
			delP.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			
		}
		});
	
	afiseazaProdus.addActionListener(new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			try {
				
				VizualizareProduse viewP = new VizualizareProduse();
				viewP.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			} catch (SQLException ex) {
				
				ex.printStackTrace();
			}
			
		}
		});
	
	comanda.addActionListener(new ActionListener() {
		
		public void actionPerformed(ActionEvent e) {
			
			AdaugareComanda addC = new AdaugareComanda();
			addC.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			
		}
		});
	
	}

	
}
