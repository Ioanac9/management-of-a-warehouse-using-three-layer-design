package presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import businessLogic.ProdusBLL;
import model.Produs;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AdaugareProdus extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private JLabel idProdusLabel;
	private JLabel denumireLabel;
	private JLabel marcaLabel;
	private JLabel cantitateLabel;
	private JLabel pretLabel;
	private JLabel observatiiLabel;
	
	private JTextField idProdusTxt;
	private JTextField denumireTxt;
	private JTextField marcaTxt;
	private JTextField cantitateTxt;
	private JTextField pretTxt;
	private JTextField observatiiTxt;
	private JButton adaugaProdusBtn;
	
	private JPanel pane = new JPanel(new GridBagLayout());
	GridBagConstraints c = new GridBagConstraints();

	public AdaugareProdus(){
		super("Adauga produs");
		c.fill=GridBagConstraints.HORIZONTAL;
		pane.setBackground (new Color(0,255,51));
		
		idProdusLabel = new JLabel("Dati id-ul produsului");
		idProdusTxt = new JTextField(40);
		idProdusLabel.setForeground(new Color(0,0,0));
		
		denumireLabel = new JLabel("Dati denumirea produsului");
		denumireLabel.setForeground(new Color(0,0,0));
		denumireTxt = new JTextField(40);
		
		marcaLabel = new JLabel("Dati marca produsului");
		marcaLabel.setForeground(new Color(0,0,0));
		marcaTxt = new JTextField(40);
		
		cantitateLabel = new JLabel("Dati cantitatea produsului");
		cantitateLabel.setForeground(new Color(0,0,0));
		cantitateTxt = new JTextField(40);
		
		pretLabel = new JLabel("Dati pretul produsului");
		pretLabel.setForeground(new Color(0,0,0));
		pretTxt = new JTextField(40);
		
		observatiiLabel=new JLabel("Observatii: ");
		
		observatiiTxt=new JTextField(40);
		observatiiTxt.setEditable(false);
		
		
		adaugaProdusBtn = new JButton("Adauga");
		adaugaProdusBtn.setBackground(new Color(51,153,255));
		adaugaProdusBtn.setForeground(new Color(0,0,0));
			
		
		c.gridx = 0;c.gridy = 0;
		pane.add(idProdusLabel,c);
		c.gridx = 0;c.gridy = 1;
		pane.add(idProdusTxt,c);
		
		c.gridx = 0;c.gridy = 2;
		pane.add(denumireLabel,c);
		c.gridx = 0;c.gridy = 3;
		pane.add(denumireTxt,c);
		
		c.gridx = 0;c.gridy = 4;
		pane.add(marcaLabel,c);
		c.gridx = 0;c.gridy = 5;
		pane.add(marcaTxt,c);
		
		c.gridx = 0;c.gridy = 6;
		pane.add(cantitateLabel,c);
		c.gridx = 0;c.gridy = 7;
		pane.add(cantitateTxt,c);
		
		c.gridx = 0;c.gridy = 8;
		pane.add(pretLabel,c);
		c.gridx = 0;c.gridy = 9;
		pane.add(pretTxt,c);
		
		c.gridx = 0;c.gridy = 10;
		pane.add(adaugaProdusBtn,c);
		
		c.gridx = 0;c.gridy = 11;
		pane.add(observatiiLabel,c);
		
		c.gridx = 0;c.gridy = 12;
		pane.add(observatiiTxt,c);
		
		
		this.add(pane);
	
		Toolkit tk = Toolkit.getDefaultToolkit();
	    Dimension screenSize = tk.getScreenSize();
		 int screenHeight = screenSize.height;
		 int screenWidth = screenSize.width;	   
		 this.setLocation(screenWidth/18, screenHeight / 8);
		
		init();
	}
	
	private void init(){
		
		
		this.setVisible(true);
		this.setSize(250, 290);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		adaugaProdusBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				int validare=1;
				String informatie = null;		
				ProdusBLL produsBll = new ProdusBLL();
				Produs produsCurent = new Produs();
				
				if(denumireTxt.getText().isEmpty() || marcaTxt.getText().isEmpty() || cantitateTxt.getText().isEmpty() 
						||idProdusTxt.getText().isEmpty() || pretTxt.getText().isEmpty()) {
					informatie="campuri goale";
					validare=0;
				}else {
				if (Integer.parseInt(cantitateTxt.getText())<=0) {
				    informatie="cantitatea < 0";
					validare=0;
				}
				if(Integer.parseInt(idProdusTxt.getText())<=0) {
				    informatie="id produs < 0";
					validare=0;
				}
				if(Integer.parseInt(pretTxt.getText())<=0) {
					informatie="pretul < 0";
					validare=0;
				}
				}
						
			    observatiiTxt.setText("");
			   
		        if(validare==0) {
		        	observatiiTxt.setText(informatie);
			        observatiiTxt.setForeground(Color.red);
					}
		        else {
				try {
					produsCurent = produsBll.insert(Integer.parseInt(idProdusTxt.getText()),denumireTxt.getText(),
							marcaTxt.getText(),Integer.parseInt(cantitateTxt.getText()),
							Integer.parseInt(pretTxt.getText()));

				} catch (Exception ex) {
					System.out.println(ex.getMessage());
				}
				observatiiTxt.setText("Date valide");
				observatiiTxt.setForeground(new Color(0,102,51));
			}
		        
			}
			});
	}
}