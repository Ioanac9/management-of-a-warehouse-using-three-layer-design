package presentation;


import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import businessLogic.ClientBLL;
import model.Client;


public class VizualizareClienti extends JFrame {

	private JScrollPane s ;
	private JPanel pane = new JPanel(new GridBagLayout());
	GridBagConstraints c = new GridBagConstraints();
	
	public VizualizareClienti() throws SQLException{
		super("Baza de date Clienti");
		Object[] names = { "idClient","Nume","Varsta"};
		List<Object> cnt = new ArrayList<Object>();
		List<Client> client1 = new ArrayList<Client>();
		ClientBLL clientBll = new ClientBLL();
		
		pane.setBackground (new Color(102,0,153));
		c.fill=GridBagConstraints.HORIZONTAL;
		
		client1 = clientBll.selectAll();
		try {
			client1 = clientBll.selectAll();
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(null,ex.getMessage());
		}
		
		for(Client it : client1)
			cnt.add(it);
			
		s = new JScrollPane(Reflection.retrieveProperties(cnt,names));
		s.setForeground(Color.blue);
		c.gridx = 0;	c.gridy = 0;
		pane.add(s,c);
		this.add(pane);
		this.setVisible(true);
		this.setSize(500,500);
		this.setLocation(0,100);
	}
	
}
