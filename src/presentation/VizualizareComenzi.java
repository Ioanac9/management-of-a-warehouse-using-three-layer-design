package presentation;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import businessLogic.ComandaBLL;
import model.Comanda;

public class VizualizareComenzi extends JFrame {

	private JScrollPane s ;
	private JPanel pane = new JPanel(new GridBagLayout());
	GridBagConstraints c = new GridBagConstraints();
	
	public VizualizareComenzi() throws SQLException{
		super("Baza de date Comenzi");
		Object[] names = { "id","numeClient","numeProdus","Cantitate"};
		List<Comanda> comanda1 = new ArrayList<Comanda>();
		List<Object> cnt = new ArrayList<Object>();
		ComandaBLL comandaBLL = new ComandaBLL();
		
		pane.setBackground (new Color(102,0,153));
		c.fill=GridBagConstraints.HORIZONTAL;
		
		try {
				comanda1 = comandaBLL.selectAll();
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(null,ex.getMessage());
		}
		
		for(Comanda it : comanda1)
			cnt.add(it);
			
		s = new JScrollPane(Reflection.retrieveProperties(cnt,names));
		c.gridx = 0;	c.gridy = 0;
		pane.add(s,c);
		this.add(pane);
		this.setVisible(true);
		this.setSize(500, 500);
		this.setLocation(500,100);
	}
	
	
	
}