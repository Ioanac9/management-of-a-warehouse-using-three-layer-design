package presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import businessLogic.ClientBLL;
import model.Client;

public class EditareClient extends JFrame{
	
	private JLabel idClientVechiLabel;
	private JLabel idClientNouLabel;
	private JLabel numeNouLabel;
	private JLabel varstaNouaLabel;
	private JLabel observatiiLabel;

	private JTextField idClientVechiTxt;
	private JTextField idClientNouTxt;
	private JTextField numeNouTxt;
	private JTextField varstaNouaTxt;
	private JTextField observatiiTxt;
		
	private JButton editeazaClientBtn;

	private JPanel pane = new JPanel(new GridBagLayout());
	GridBagConstraints c = new GridBagConstraints();
	
	public EditareClient(){
		super("Editeaza client");
        c.fill=GridBagConstraints.HORIZONTAL;
        pane.setBackground (new Color(102,51,0));
        
        c.gridx = 0;c.gridy = 0;
		idClientVechiLabel = new JLabel("Vechiul id:");
		idClientVechiLabel.setForeground(Color.white);
		pane.add(idClientVechiLabel,c);
		
		c.gridx = 0;c.gridy = 1;
		idClientVechiTxt = new JTextField(40);
		pane.add(idClientVechiTxt,c);
		
		c.gridx = 0;c.gridy = 2;
		idClientNouLabel = new JLabel("Noul id:");
		idClientNouLabel.setForeground(Color.white);
		pane.add(idClientNouLabel,c);
		
		c.gridx = 0;c.gridy = 3;
		idClientNouTxt = new JTextField(40);
		pane.add(idClientNouTxt,c);
		
		c.gridx = 0;c.gridy = 4;
		numeNouLabel = new JLabel("Noul nume:");
		numeNouLabel.setForeground(Color.white);
		pane.add(numeNouLabel,c);
		
		c.gridx = 0;c.gridy = 5;
		numeNouTxt = new JTextField(40);
		pane.add(numeNouTxt,c);
		
		c.gridx = 0;c.gridy = 6;
		varstaNouaLabel = new JLabel("Noua varsta:");
		varstaNouaLabel.setForeground(Color.white);
		pane.add(varstaNouaLabel,c);
		
		c.gridx = 0;c.gridy =7;
		varstaNouaTxt = new JTextField(40);
		pane.add(varstaNouaTxt,c);
		
		
		c.gridx = 0;c.gridy = 8;
		editeazaClientBtn = new JButton("Editeaza");
		editeazaClientBtn.setBackground(new Color(255,255,153));
		pane.add(editeazaClientBtn,c);
		
		c.gridx = 0;c.gridy = 9;
		observatiiLabel=new JLabel("Observatii: ");
		observatiiLabel.setForeground(Color.cyan);
		pane.add(observatiiLabel,c);
		
		c.gridx = 0;c.gridy = 10;
		observatiiTxt=new JTextField(40);
		pane.add(observatiiTxt,c);
		observatiiTxt.setEditable(false);
		
		this.add(pane);
		Toolkit tk = Toolkit.getDefaultToolkit();
	    Dimension screenSize = tk.getScreenSize();
		 int screenHeight = screenSize.height;
		 int screenWidth = screenSize.width;	   
		 this.setLocation(screenWidth/14 , screenHeight / 2);
		init();
	}
	
	private void init(){
		
		this.setVisible(true);
		this.setSize(220, 250);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		editeazaClientBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				int validare=1;
				String informatie=null;				
			
				ClientBLL clientBll = new ClientBLL();
				Client clientCurent = new Client();
				
				if(numeNouTxt.getText().isEmpty() || varstaNouaTxt.getText().isEmpty() ||
						idClientVechiTxt.getText().isEmpty()||idClientNouTxt.getText().isEmpty()) {
					informatie="campuri goale";
					validare=0;
				}else {
				if(Integer.parseInt(varstaNouaTxt.getText())<=0 ||Integer.parseInt(varstaNouaTxt.getText())>=120){
				    informatie="varsta invalida";
					validare=0;
				}
				if(Integer.parseInt(idClientVechiTxt.getText())<=0||Integer.parseInt(idClientNouTxt.getText())<=0) {
					informatie="id Client < 0";
					validare=0;
				}
				}
						
			    observatiiTxt.setText("");
			    if(validare==0)  {
			    	observatiiTxt.setText(informatie);
			        observatiiTxt.setForeground(Color.red);
			        }else {
			    
				try {
					clientCurent = clientBll.update(Integer.parseInt(idClientNouTxt.getText()),numeNouTxt.getText(),
							Integer.parseInt(varstaNouaTxt.getText()),Integer.parseInt(idClientVechiTxt.getText()));

				} catch (Exception ex) {
					System.out.println(ex.getMessage());
				}
				observatiiTxt.setText("Date valide");
				observatiiTxt.setForeground(Color.GREEN);
			}
			}
			});
		
		
	}
}