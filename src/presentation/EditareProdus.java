package presentation;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import businessLogic.ProdusBLL;
import model.Produs;


public class EditareProdus extends JFrame{

	private JLabel idProdusVechiLabel;
	private JLabel idProdusNouLabel;
	private JLabel denumireNouaLabel;
	private JLabel marcaNouaLabel;
	private JLabel cantitateNouaLabel;
	private JLabel pretNouLabel;
	private JLabel observatiiLabel;

	private JTextField idProdusVechiTxt;
	private JTextField idProdusNouTxt;
	private JTextField denumireNouaTxt;
	private JTextField marcaNouaTxt;
	private JTextField cantitateNouaTxt;
	private JTextField pretNouTxt;
	private JTextField observatiiTxt;
	
	private JButton editeazaProdusBtn;
	
	private JPanel pane = new JPanel(new GridBagLayout());
	GridBagConstraints c = new GridBagConstraints();
	public EditareProdus(){
		super("Editeaza produs");
		c.fill=GridBagConstraints.HORIZONTAL;
		pane.setBackground (new Color(153,102,0));
		
		idProdusNouLabel= new JLabel("Dati noul id: ");
		idProdusNouLabel.setForeground(Color.white);
		idProdusNouTxt = new JTextField(40);
		
	    denumireNouaLabel = new JLabel("Dati noua denumire: ");
		denumireNouaLabel.setForeground(Color.white);
		denumireNouaTxt = new JTextField(40);
		
		marcaNouaLabel = new JLabel("Dati noua marca: ");
		marcaNouaLabel.setForeground(Color.white);
		marcaNouaTxt= new JTextField(40);
		
		cantitateNouaLabel = new JLabel("Dati noua cantitate: ");
		cantitateNouaLabel.setForeground(Color.white);
		cantitateNouaTxt = new JTextField(40);
		
		pretNouLabel = new JLabel("Dati noul pret: ");
		pretNouLabel.setForeground(Color.white);
		pretNouTxt = new JTextField(40);
		
		idProdusVechiLabel = new JLabel("Dati vechiul id: ");
		idProdusVechiLabel.setForeground(Color.white);
		idProdusVechiTxt = new JTextField(40);
		editeazaProdusBtn = new JButton("Editeaza");
		
		c.gridx = 0;c.gridy = 0;
		pane.add(idProdusVechiLabel,c);
		c.gridx = 0;c.gridy = 1;
		pane.add(idProdusVechiTxt,c);
		c.gridx = 0;c.gridy = 2;
		pane.add(idProdusNouLabel,c);
		c.gridx = 0;c.gridy = 3;
		pane.add(idProdusNouTxt,c);
		c.gridx = 0;c.gridy = 4;
		pane.add(denumireNouaLabel,c);
		c.gridx = 0;c.gridy = 5;
		pane.add(denumireNouaTxt,c);
		c.gridx = 0;c.gridy = 6;
		pane.add(marcaNouaLabel,c);
		c.gridx = 0;c.gridy = 7;
		pane.add(marcaNouaTxt,c);
		c.gridx = 0;c.gridy = 8;
		pane.add(cantitateNouaLabel,c);
		c.gridx = 0;c.gridy = 9;
		pane.add(cantitateNouaTxt,c);
		c.gridx = 0;c.gridy = 10;
		pane.add(pretNouLabel,c);
		c.gridx = 0;c.gridy = 11;
		pane.add(pretNouTxt,c);
		
		c.gridx = 0;c.gridy = 12;
		pane.add(editeazaProdusBtn,c);
		editeazaProdusBtn.setBackground(new Color(255,204,51));
		editeazaProdusBtn.setForeground(new Color(0,0,0));
		
		c.gridx = 0;c.gridy = 13;
		observatiiLabel=new JLabel("Observatii: ");
		observatiiLabel.setForeground(Color.cyan);
		pane.add(observatiiLabel,c);
		
		c.gridx = 0;c.gridy = 14;
		observatiiTxt=new JTextField(40);
		pane.add(observatiiTxt,c);
		observatiiTxt.setEditable(false);
		
		this.add(pane);
		
		this.setLocation(920, 100);
		init();
	}
	
	private void init(){
		
		
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(250, 340);
		
		editeazaProdusBtn.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				int validare=1;
				String informatie=null;			

				ProdusBLL produsBll = new ProdusBLL();	
				Produs produsCurent = new Produs();
				
				if(idProdusNouTxt.getText().isEmpty() || denumireNouaTxt.getText().isEmpty() ||
						marcaNouaTxt.getText().isEmpty()||cantitateNouaTxt.getText().isEmpty()||
						pretNouTxt.getText().isEmpty()||idProdusVechiTxt.getText().isEmpty()) {
					informatie="campuri goale";
					validare=0;
				}else {
				if(Integer.parseInt(idProdusNouTxt.getText())<=0 ||Integer.parseInt(idProdusVechiTxt.getText())<=0){
				    informatie="id produs < 0";
					validare=0;
				}
				if(Integer.parseInt(pretNouTxt.getText())<=0) {
					informatie="pret < 0";
					validare=0;
				}
				if(Integer.parseInt(cantitateNouaTxt.getText())<=0) {
					informatie="cantitate < 0";
					validare=0;
				}
				}
						
			    observatiiTxt.setText("");
			   
			    if(validare==0)  {
			        observatiiTxt.setText(informatie);
			        observatiiTxt.setForeground(Color.red);
			        }else {
				try {
					produsCurent = produsBll.update(Integer.parseInt(idProdusNouTxt.getText()),
							denumireNouaTxt.getText(),marcaNouaTxt.getText(), Integer.parseInt(cantitateNouaTxt.getText())
							,Integer.parseInt(pretNouTxt.getText()),Integer.parseInt(idProdusVechiTxt.getText()));

				} catch (Exception ex) {
					System.out.println(ex.getMessage());
				}
				observatiiTxt.setText("Date valide");
				observatiiTxt.setForeground(Color.GREEN);
			}
			}
			});
	}
	
	
}