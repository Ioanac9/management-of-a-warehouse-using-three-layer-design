package presentation;

import java.lang.reflect.Field;
import java.util.List;

import javax.swing.JTable;

public class Reflection{
	 //se extrag datele din tabela 
	//valori,numele campurilor
	public static JTable retrieveProperties(List<Object> object,Object[] names) {
		Object [][] v = new Object[object.size()][names.length];//se creaza un tabel de dimensiunea valorilor care se adauga si numarul de campuri din tabela
		int i=0;
		int j=0;
		for(Object it : object){//pentru fiecare data 
			for (Field field : it.getClass().getDeclaredFields()) { //se identifica campul in care se afla
				field.setAccessible(true); //seteaza data accesibila
				Object value;
				
				try {
					value = field.get(it);//se ia data din campul corespunzator 
					v[i][j]=value;// se adauga valoarea gasita in matricea de date
					j++;
					
	
				} catch (Exception e) {
					e.printStackTrace();
				}		
			}
			i++;
			j=0;
		}
		
		JTable t = new JTable(v,names); // se creeaza tabela de afisare prin matricea de date construita si cu ca
		return t;
	}
}