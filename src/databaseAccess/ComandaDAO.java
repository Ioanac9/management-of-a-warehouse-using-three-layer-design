package databaseAccess;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Client;
import model.Comanda;


public class ComandaDAO extends AbstractDAO<Comanda>{

	public List<Comanda> selectAll(){
		return super.selectAll();
	}
	public Comanda insert(int id,String numeClient,String numeProdus,int cantitate) throws SQLException{
		ArrayList<Object> list = new ArrayList<Object>();
		list.add(id);
		list.add(numeClient);
		list.add(numeProdus);
		list.add(cantitate);
		return super.insert(list);
	}
	
}
