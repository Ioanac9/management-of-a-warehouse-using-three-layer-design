package databaseAccess;



import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Produs;



public class ProdusDAO extends AbstractDAO<Produs>{

	
	public Produs findById(int id) throws SQLException{
		return super.findById(id,"idProdus");
	}
	
	public List<Produs> selectAll(){
		return super.selectAll();
	}
	
	public Produs insert(int id,String denumire,String marca,int cantitate,int pret) throws SQLException{
		ArrayList<Object> list = new ArrayList<Object>();
		list.add(id);
		list.add(denumire);
		list.add(marca);
		list.add(cantitate);
		list.add(pret);
		return super.insert(list);
	}
	//se creeaza lista de obiecte care se doresc a se modifica in tabela Produs si lista de campuri din bd 
	public Produs update(Integer newId,String newDenumire,String newMarca,Integer newCantitate,Integer pret,int id) throws SQLException{
		ArrayList<String> names = new ArrayList<String>();
		ArrayList<Object> list = new ArrayList<Object>();
		list.add(newId);
		list.add(newDenumire);
		list.add(newMarca);
		list.add(newCantitate);
		list.add(pret);
		names.add("idProdus");
		names.add("Denumire");
		names.add("Marca");
		names.add("Cantitate");
		names.add("Pret");
		return super.update(list,names,id,"idProdus");
	}
	
	public Produs delete(String name,Integer id) throws SQLException{
		ArrayList<String> names = new ArrayList<String>();
		ArrayList<Object> list = new ArrayList<Object>();
		
			list.add(id);
			list.add(name);
			names.add("idProdus");
			names.add("Denumire");
		
		
		return super.delete(list, names);			
	}
	
}
