package databaseAccess;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Client;


public class ClientDAO extends AbstractDAO<Client> {
	
	public List<Client> selectAll(){
		return super.selectAll();
	}
	//se apeleaza metoda prin care se cauta id-ul unui client in campul idClient din tabela Client din baza de date
	public Client findById(int id) throws SQLException{
		return super.findById(id,"idClient");
	}
   //se adauga intr-o lista datele dorite a se insera in tabela Client din baza de date
	public Client insert(Integer id,String nume,Integer varsta) throws SQLException{
		ArrayList<Object> list = new ArrayList<Object>();
		list.add(id);
		list.add(nume);
		list.add(varsta);
		return super.insert(list);
	}
	//se creeaza o lista cu datele pe care dorim sa le inlocuim si o lista cu numele campurilor pe care dorim sa le inlocuim
	public Client update(Integer newId,String newName,Integer newVarsta,int id) throws SQLException{
		ArrayList<String> names = new ArrayList<String>();
		ArrayList<Object> list = new ArrayList<Object>();
		list.add(newId);
		list.add(newName);
		list.add(newVarsta);
		names.add("idClient");
		names.add("nume");
		names.add("varsta");
		return super.update(list,names,id,"idClient");
	}
	
	public Client delete(String name,Integer id) throws SQLException{
		ArrayList<String> names = new ArrayList<String>();
		ArrayList<Object> list = new ArrayList<Object>();
				
			list.add(id);
			list.add(name);
			names.add("idClient");
			names.add("nume");
	
		return super.delete(list, names);			
	}
	
	
	
}
