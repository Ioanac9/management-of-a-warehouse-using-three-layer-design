package businessLogic;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import databaseAccess.ProdusDAO;
import model.Produs;



public class ProdusBLL {

	private ProdusDAO produsDAO;
	
	public ProdusBLL(){
		produsDAO = new ProdusDAO();
	}
	
	public List<Produs> selectAll() {
		List<Produs> pr = new ArrayList<Produs>();
		pr = produsDAO.selectAll();
		if (pr == null) {
			throw new NoSuchElementException(" Nu s-au gasit produse!");
		}
		return pr;
	}
	
     	public Produs findById(int id)  {
		Produs pr = null;
		try {
			pr = produsDAO.findById(id);
			if (pr == null) {
				throw new NoSuchElementException("Produsul cu id-ul =" + id + " nu a fost gasit!");				
			}
		} catch (SQLException e) {
			e.getMessage();
		}
		return pr;		
	}
	
	public Produs insert(Integer id,String denumire,String marca,Integer cantitate,Integer pret)  {
		Produs pr = null;
		try {
			pr = produsDAO.insert(id,denumire,marca,cantitate,pret);
			if (pr == null) {
				throw new NoSuchElementException("Produsul cu id-ul =" + id + " nu s-a inserat!");				
			}
		} catch (SQLException e) {
			e.getMessage();
		}
		return pr;
				
	}
	
	public Produs update(Integer newId,String newDenumire,String newMarca,Integer newCantitate,Integer pret,int id) {
		Produs pr = null;
		try {
			pr = produsDAO.update(newId,newDenumire,newMarca,newCantitate,pret,id);
			if (pr == null) {
				throw new NoSuchElementException("Produsul cu id-ul =" + id + " nu s-a actualizat!");				
			}
		} catch (SQLException e) {
			
			e.getMessage();
		}
		
		return pr;
	}
	
	public Produs delete(int id,String name) {
		Produs pr = null;
		try {
			pr = produsDAO.delete(name,id);
			if (pr == null) {

				throw new NoSuchElementException("Produsul cu id-ul=" + id + " nu a fost sters!");
						}
		} catch (SQLException e) {
			
			e.getMessage();
		}	
		return pr;
	}
	
	
	
}