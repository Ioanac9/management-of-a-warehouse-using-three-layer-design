package businessLogic;

import java.util.NoSuchElementException;
import javax.swing.JOptionPane;
import databaseAccess.ClientDAO;
import model.Client;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class ClientBLL {
	private ClientDAO clientDAO;

	public ClientBLL() {
		clientDAO = new ClientDAO();
	}

	public List<Client> selectAll() throws SQLException {
		List<Client> listaClienti = new ArrayList<Client>();
		listaClienti=clientDAO.selectAll();
		if (listaClienti == null) {
			JOptionPane.showMessageDialog(null,"Nu se gasesc clienti!");
			throw new NoSuchElementException("Nu se gasesc clienti!");
		}
		return listaClienti;
	}
	public Client insert(Integer id,String nume,Integer varsta) {
		Client clientCurent = null;
		try {
			clientCurent = clientDAO.insert(id,nume,varsta);
			if (clientCurent == null) {
				JOptionPane.showMessageDialog(null,"Clientul cu id-ul " + id +" nu a fost inserat!");
				throw new NoSuchElementException("Clientul cu id-ul " + id +" nu a fost inserat!");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return clientCurent;
	}
	
	public Client update(Integer noulId,String noulNume,Integer nouaVarsta,int vechiulId) {
		Client clientCurent = null;

		try {
			
			clientCurent = clientDAO.update(noulId,noulNume,nouaVarsta,vechiulId);
			if (clientCurent == null) {
				JOptionPane.showMessageDialog(null,"Clientul cu id-ul " + vechiulId +" nu a fost actualizat!");
				throw new NoSuchElementException("Clientul cu id-ul " + vechiulId +" nu a fost actualizat!");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return clientCurent;
	}
	
	public Client delete(int id,String name) {
		Client clientCurent = null;
		try {
			clientCurent = clientDAO.delete(name,id);
			if (clientCurent == null) {
				//JOptionPane.showMessageDialog(null,"Clientul cu id-ul " + id +" nu a fost sters!");
				throw new NoSuchElementException("Clientul cu id-ul " + id +" nu a fost sters!");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return clientCurent;
	}
	
	public Client findClientById(int id){
		Client clientCurent = null;
		try {
			clientCurent = clientDAO.findById(id);
			if (clientCurent == null) {
				JOptionPane.showMessageDialog(null,"Clientul cu id-ul " + id +" nu a fost gasit!");
				throw new NoSuchElementException("Clientul cu id-ul " + id +" nu a fost gasit!");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return clientCurent;
	}
}