package businessLogic;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import databaseAccess.ClientDAO;
import databaseAccess.ComandaDAO;
import databaseAccess.ProdusDAO;
import model.Client;
import model.Comanda;
import model.Produs;

public class ComandaBLL {
	
	private ComandaDAO comandaDAO;
	
	public ComandaBLL(){
		comandaDAO = new ComandaDAO();
	}	
	
	public Comanda comandaNoua(int idClient,int idProdus,int cantitate,Integer id) {
		Client clientCurent= new Client();
		ClientDAO clientCurentDAO = new ClientDAO();
		Produs produsCurent = new Produs();
		ProdusDAO produsCurentDAO = new ProdusDAO();
		
		try {
			clientCurent = clientCurentDAO.findById(idClient);
			produsCurent = produsCurentDAO.findById(idProdus);
			return comandaDAO.insert(id,clientCurent.getNume(),produsCurent.getDenumire(),cantitate);
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		return null;
				
	}

	public List<Comanda> selectAll() {
		List<Comanda> listaComenzi = new ArrayList<Comanda>();
		listaComenzi= comandaDAO.selectAll();
		if (listaComenzi == null) {
			throw new NoSuchElementException("Nu se gasesc clienti!");
		}
		return listaComenzi;
	}
	
}