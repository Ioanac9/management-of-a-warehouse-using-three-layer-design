package model;

public class Comanda {
	private int idComenzi;
	private String numeClient;
	private String numeProdus;
	private int Cantitate;
	
	public Comanda(){
		idComenzi=0;
		numeClient=null;
		numeProdus=null;
		Cantitate=0;		
	}
	
	public Comanda (int idComenzi,String numeClient, String numeProdus,int Cantitate){
		this.idComenzi = idComenzi;
		this.numeClient = numeClient;
		this.numeProdus = numeProdus;
		this.Cantitate = Cantitate;
	}
	public int getIdComenzi() {
		return idComenzi;
	}
	
	public String getNumeClient() {
		return numeClient;
	}
	
	public int getCantitate() {
		return Cantitate;
	}
	
	public String getNumeProdus() {
		return numeProdus;
	}
	
	public void setNumeClient(String numeClient) {
		this.numeClient = numeClient;
	}
	
	public void setCantitate(int cantitate) {
		Cantitate = cantitate;
	}
	
	public void setIdComenzi(int idComenzi) {
		this.idComenzi = idComenzi;
	}
	
	public void setNumeProdus(String numeProdus) {
		this.numeProdus = numeProdus;
	}
	
}