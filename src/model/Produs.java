package model;

public class Produs {
	private int idProdus;
	private String Denumire;
	private String Marca;
	private int Cantitate;
	private int Pret;
	
	public Produs(){
		idProdus=0;
		Denumire=null;
		Marca=null;
		Cantitate=0;
		Pret=0;
	}
	
	public Produs(int idP,String D,String M,int C,int P){
		idProdus = idP;
		Denumire = D;
		Marca = M;
	    Cantitate = C;
		Pret = P;
	}
	
	public int getIdProdus() {
		return idProdus;
	}
	
	public String getDenumire() {
		return Denumire;
	}
	
	public String getMarca() {
		return Marca;
	}
	
	public int getCantitate() {
		return Cantitate;
	}
	
	public int getPret() {
		return Pret;
	}
	
	
	public void setIdProdus(int idProduse) {
		this.idProdus = idProduse;
	}
	
	public void setDenumire(String denumire) {
		Denumire = denumire;
	}
	
	public void setMarca(String marca) {
		Marca = marca;
	}
	
	public void setCantitate(int cantitate) {
		Cantitate = cantitate;
	}
	
	public void setPret(int pret) {
		Pret = pret;
	}
}