package model;

public class Client {

	private int idClient;
	private String nume;
	private int varsta;
	
	public Client(){}
	
	public Client (int id,String nume,int varsta){
		this.idClient = id;
		this.nume = nume;
		this.varsta = varsta;
	}
	
	public int getIdClient() {
		return idClient;
	}
	
	public int getVarsta() {
		return varsta;
	}
	
	public String getNume() {
		return nume;
	}
	
	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
	
	public void setVarsta(int varsta) {
		this.varsta = varsta;
	}
	
	public void setNume(String nume) {
		this.nume = nume;
	}
}
