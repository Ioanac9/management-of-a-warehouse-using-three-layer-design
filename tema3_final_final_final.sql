SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';


-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`client` (
  `idClient` INT(11) NOT NULL,
  `nume` VARCHAR(45) NULL DEFAULT NULL,
  `varsta` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`idClient`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `mydb`.`comenzi`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`comenzi` (
  `idComenzi` INT(11) NOT NULL,
  `numeClient` VARCHAR(20) NULL DEFAULT NULL,
  `numeProdus` VARCHAR(20) NULL DEFAULT NULL,
  `cantitate` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
  
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `mydb`.`produse`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`Produse` (
  `idProdus` INT(11) NOT NULL,
  `Denumire` VARCHAR(45) NULL DEFAULT NULL,
  `Marca` VARCHAR(45) NULL DEFAULT NULL,
  `Cantitate` INT(11) NULL DEFAULT NULL,
  `Pret` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`idProdus`))
  
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
